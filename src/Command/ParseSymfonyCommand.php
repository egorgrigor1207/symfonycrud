<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use App\Entity\NamespaceSymfony;

class ParseSymfonyCommand extends Command
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var
     */
    private $url = 'https://api.symfony.com/4.1/index.html';

    /**
     * ParseSymfonyCommand constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this
            ->setName('app:parsesymfony')
            ->setDescription('Parsing site api.symfony.com')
            ->setHelp('This command parses the site ...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContent('http://api.andreybolonin.com');
    }

    /**
     * @param $url= 'https://api.symfony.com/4.1/index.html'
     */
    public function getContent($url)
    {
        $html = file_get_contents($url);

        $crawler = new Crawler($html);
//        $crawler->addHtmlContent($html, 'UTF-8');
        $filtered = $crawler->filter('div.namespace-container > ul > li > a');
        //var_dump($filtered->count());
        foreach ($filtered as $value) {
            var_dump($value->textContent);
            $url = 'https://api.symfony.com/4.1/'.str_replace('../', '', $value->getAttribute('href'));
            $set = new NamespaceSymfony();
            $set->setName($value->textContent);
            $set->setUrl($url);
            $this->em->persist($set);
        }
        $this->em->flush();
    }
}

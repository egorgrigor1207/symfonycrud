<?php

declare(strict_types=1);

namespace App\Command;
use App\Entity\ClassSymfony;
use App\Entity\InterfaceSymfony;
use App\Entity\NamespaceSymfony;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class ParseNamespacesCommand.
 */

class ParseNamespacesCommand extends Command
{
    /** @var string */
    private const API_URL = 'http://api.andreybolonin.com/';

    /** @var string */
    protected static $defaultName = 'app:parse-symfony';

    /** @var EntityManagerInterface */
    private $em;

    /**
     * ParseNamespacesCommand constructor.
     */

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    protected function configure()
    {
        $this->setDescription('Parsing site api.andreybolonin.com')
            ->setHelp('This command will parse the site api.andreybolonin.com and save it to DB...')
            ->setDefinition(new InputDefinition(
                [
                    new InputOption('test', 't', InputOption::VALUE_OPTIONAL),
                ]
            ));
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $client = HttpClient::create();
        $html = $client->request('GET', 'http://api.andreybolonin.com');

        $crawler = new Crawler($html->getContent());
        $elements = $crawler->filter('div#page-content > div.namespaces.clearfix > div.namespace-container > ul > li > a');
        foreach ($elements as $element) {
            $namespace = new NamespaceSymfony();
            $namespace->setName($element->textContent);
            $namespace->setUrl('http://api.andreybolonin.com/' . $element->getAttribute('href'));
            $this->em->persist($namespace);

            $client = HttpClient::create();
           $html = $client->request('GET', $namespace->getUrl());

                $crawler = new Crawler($html->getContent());
                $InterfaceElements = $crawler->filter('div#page-content > div.container-fluid.underlined > div.row > div.col-md-6 > em > a > abbr');
                foreach ($InterfaceElements as $elementInterface) {
                    $interface = new InterfaceSymfony();
                    $interface->setNamespaceSymfony($namespace);
                    $interface->setName($elementInterface->textContent);
                    $interface->setUrl('http://api.andreybolonin.com/' . $elementInterface->getAttribute('href'));
                    $this->em->persist($interface);
//                   var_dump($InterfaceElements->count());
                }
            }
           $this->em->flush();

          return 1;
        }
    }
<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NamespaceSymfonyRepository")
 */
class NamespaceSymfony
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $url;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\InterfaceSymfony", mappedBy="namespaceSymfony")
     */
    private $interfacesSymfony;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ClassSymfony", mappedBy="namespaceSymfony")
     */
    private $classesSymfony;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): NamespaceSymfony
    {
        $this->name = $name;

        return $this;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setUrl(string $url): NamespaceSymfony
    {
        $this->url = $url;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): NamespaceSymfony
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|InterfaceSymfony[]
     */
    public function getInterfacesSymfony(): Collection
    {
        return $this->interfacesSymfony;
    }

    public function addInterfacesSymfony(InterfaceSymfony $interfacesSymfony): NamespaceSymfony
    {
        if (!$this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony[] = $interfacesSymfony;
            $interfacesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeInterfacesSymfony(InterfaceSymfony $interfacesSymfony): NamespaceSymfony
    {
        if ($this->interfacesSymfony->contains($interfacesSymfony)) {
            $this->interfacesSymfony->removeElement($interfacesSymfony);
            // set the owning side to null (unless already changed)
            if ($interfacesSymfony->getNamespaceSymfony() === $this) {
                $interfacesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ClassSymfony[]
     */
    public function getClassesSymfony(): Collection
    {
        return $this->classesSymfony;
    }

    public function addClassesSymfony(ClassSymfony $classesSymfony): NamespaceSymfony
    {
        if (!$this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony[] = $classesSymfony;
            $classesSymfony->setNamespaceSymfony($this);
        }

        return $this;
    }

    public function removeClassesSymfony(ClassSymfony $classesSymfony): NamespaceSymfony
    {
        if ($this->classesSymfony->contains($classesSymfony)) {
            $this->classesSymfony->removeElement($classesSymfony);
            // set the owning side to null (unless already changed)
            if ($classesSymfony->getNamespaceSymfony() === $this) {
                $classesSymfony->setNamespaceSymfony(null);
            }
        }

        return $this;
    }

    public function getParent(): ?NamespaceSymfony
    {
        return $this->parent;
    }

    public function setParent(?NamespaceSymfony $parent): NamespaceSymfony
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|NamespaceSymfony[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(NamespaceSymfony $child): NamespaceSymfony
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(NamespaceSymfony $child): NamespaceSymfony
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }
}
